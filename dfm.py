#!/usr/bin/python
#coding: utf-8
from math import sqrt, pi
from time import clock

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.lines as lines
import matplotlib.font_manager  as fnt
from discretisation import discretise_contour, sigmoid

from tests import run_tests
from tests.test_dfm import *
from utils import curve_lenght

def restore_settings():
    settings = {
        'c' : 100.,
        'Vinf' : [-1., 0.],
        'eps' : 1E-10,
        'verbosity' : 1,
        'contour_function' : 'default',
        'make_regularisation' : True,
        'nose_length' : 0.3,
        'tail_length' : 0.7,
        'discretisator_settings' : {
            'type' : 'x_axis_uniform',
            # for x-axis-uniform
            'coefficient' : 50,
            # for x-axis-nonuniform and contour-nonuniform            
            'hmax' : 0.1,
            'hmin' : 0.05,
            'transition_length' : 0.6,
            }
        }
    return settings
        
def W(M, x):
    """Speed function"""
    coef = 1 / (2 * pi) / ((M[0] - x[0]) ** 2 + (M[1] - x[1]) ** 2)
    return -1 * coef * (M[1] - x[1]), coef * (M[0] - x[0])

def calculate_control_points(wirldwind_points):
    control_points, control_points_ortonormals = list(), list()
    for i, p in enumerate(wirldwind_points):
        next_p = wirldwind_points[i+1] if i != len(wirldwind_points) - 1 else wirldwind_points[0]
        control_point = [0, 0]
        control_point[0] = 0.5 * (p[0] + next_p[0])
        control_point[1] = 0.5 * (p[1] + next_p[1])
        control_points.append(control_point)

        vect = [control_point[0] - p[0], control_point[1] - p[1]]
        norm = [vect[1], - vect[0]]
        #FIXME reduce with pow lambda
        norm_len = sqrt(norm[0] ** 2 + norm[1] ** 2)
        norm[0] /= norm_len
        norm[1] /= norm_len
        control_points_ortonormals.append(norm)
    return control_points, control_points_ortonormals

def fill_laes(Vinf, wirldwind_points, control_points, control_points_ortonormals, make_regularisation=True):
    A, b = list(), list()
    laes_size = len(control_points)
    
    for i in xrange(laes_size):
        n = control_points_ortonormals[i]
        b_elem = - np.dot(Vinf, n)
        b.append(b_elem)
        A_row = list()
        wirldwind_p = wirldwind_points[i]
        if make_regularisation:
            cols_amount  = laes_size - 1
        else:
            cols_amount  = laes_size             
        for j in xrange(cols_amount):
            control_p = control_points[j]
            W_val = W(control_p, wirldwind_p)
            a_coef = np.dot(W_val, n)
            A_row.append(a_coef)
        if make_regularisation:
            A_row.append(1)        
        A.append(A_row)
    return A, b        

def solve_laes(A, b):
    return np.linalg.solve(np.array(A), np.array(b))

def calculate_control_points_speeds(settings, gamma, wirldwind_points, control_points):
    control_points_speeds = list()
    for wirldwind_p in wirldwind_points:
        # reinit current_speed with every iteration
        current_speed = settings['Vinf'][:]
        for i  in xrange(len(control_points) - 1):
            control_p = control_points[i]
            W_val = W(control_p, wirldwind_p)
            current_speed[0] += gamma[i] * W_val[0]
            current_speed[1] += gamma[i] * W_val[1]
            if settings['verbosity'] == 3:
                print ' speed traceback: g=%0.2f, W=%s, s=%s (i=%s)'%(gamma[i], W_val, current_speed, i)
        control_points_speeds.append(current_speed)
    return control_points_speeds

def point_contour_length(point_number, control_points):
    lenght = 0
    for i in xrange(1, point_number):
        vector = [control_points[i][0] - control_points[i - 1][0],
                  control_points[i][1] - control_points[i - 1][1],]
        lenght += sqrt(vector[0] ** 2 + vector[1] ** 2)
    return lenght

def plot_results(wirldwind_points, control_points, control_points_speeds, control_points_ortonormals,
                 legend_note, draw_normals=True, draw_speed_directions=True, draw_speed_graph=True, subplot=None):
    if not subplot:
        wing_fingure = plt.figure()
        subplot = wing_fingure.add_subplot(111)
    x_vals = [p[0] for p in wirldwind_points] + [wirldwind_points[0][0]]
    y_vals = [p[1] for p in wirldwind_points] + [wirldwind_points[0][1]]
    wing, = subplot.plot(x_vals, y_vals, 'o-', label=legend_note, ms=5)
    if draw_normals:
        for i, c in enumerate(control_points):
            x_vals = [c[0], c[0] + control_points_ortonormals[i][0]]
            y_vals = [c[1], c[1] + control_points_ortonormals[i][1]]
            line = lines.Line2D(x_vals, y_vals)
            subplot.text(c[0], c[1], str(control_points_ortonormals[i]))
            subplot.add_line(line)
    if draw_speed_directions:
        for i, c in enumerate(control_points):
            x_vals = [c[0], c[0] + control_points_speeds[i][0]]
            y_vals = [c[1], c[1] + control_points_speeds[i][1]]
            subplot.plot(x_vals, y_vals)
    else:
        for i, c in enumerate(control_points):
            p = control_points_speeds[i]
            if abs(i % (len(control_points) / 10)) < 1E-3:
                text = '(%0.2f, %0.2f)'%(-p[0], p[1])
                subplot.text(c[0], c[1], text,
                             withdash=True, dashlength=20, dashrotation=90)
    subplot.margins(0.1,0.1)
    subplot.grid(True)

def plot_speed_results(control_points, control_points_speeds, legend_note, speed_subplot=None):
    if not speed_subplot:
        speed_pts = plt.figure()
        speed_subplot = speed_pts.add_subplot(111)
    x_vals = [point_contour_length(i, control_points) for i, c in enumerate(control_points)]
    y_vals = [-p[0] for p in control_points_speeds]
    speed_subplot.plot(x_vals, y_vals, label=legend_note)
    speed_subplot.margins(0.1,0.1)
    speed_subplot.grid(True)

def check_solution_is_tangent(control_points_ortonormals, control_points_speeds):
    """Checks if speeds are tangent.
       Returns max of scalar multiplications."""
    normal_disp = []
    for i, n in enumerate(control_points_ortonormals):
        normal_disp.append(np.dot(n, control_points_speeds[i]))
    return max(normal_disp)

def plot_h_step_example():
    """Draws x_axis_nonuniform_step_function.
       For debug purposes."""
    alpha, beta = 1, 6
    dataset_pts = plt.figure()
    subplot = dataset_pts.add_subplot(111)
    hmin = 0.1
    hmax = 0.5
    nose_length = 0.25
    tail_length = 0.75
    transition_length = 0.3
    x_vals = [0.001 * i for i in xrange(1000)]
    y_vals = [x_axis_nonuniform_step_function(x, hmin, hmax, nose_length, tail_length, transition_length) for x in x_vals]
    line = lines.Line2D(x_vals, y_vals)
    subplot.add_line(line)
    subplot.set_xlim(0, 1)
    subplot.set_ylim(0, hmax + 0.1)
    subplot.grid(True)

def run_mds(settings, subplot=None, speed_subplot=None, draw_speed_graph=None):
        wirldwind_points = discretise_contour(settings)
        control_points, control_points_ortonormals = calculate_control_points(wirldwind_points)
        A, b = fill_laes(settings['Vinf'], wirldwind_points, control_points,
                         control_points_ortonormals, make_regularisation=settings['make_regularisation'])
        gamma = solve_laes(A, b)
        if settings['verbosity'] >= 1:
            print '\tlaes size = %s'%len(b)
        if settings['verbosity'] >= 2:
            for row in A:
                print ['%0.2f'%e for e in row]
            print '\tVECTOR', ['%0.2f'%e for e in b]
            print '\tSOLUTION', ['%0.2f'%e for e in gamma]
        if settings['verbosity'] >= 1:
            print '\tdisp =', np.linalg.norm(np.sum([np.dot(A,gamma), [-e for e in b]]))
        control_points_speeds = calculate_control_points_speeds(settings, gamma, wirldwind_points, control_points)
        if settings['verbosity'] >= 2:
            for i, s in enumerate(control_points_speeds):
                print '\t(%0.2f, %0.2f) [%0.2f, %0.2f]'%(control_points[i][0], control_points[i][1], s[0], s[1])
        normal_disp = check_solution_is_tangent(control_points_ortonormals, control_points_speeds)
        if settings['verbosity'] >= 2:
            print '\tmax of normal disps = %s'%normal_disp
        legend_note = settings['discretisator_settings']['type']
        plot_results(wirldwind_points, control_points, control_points_speeds, control_points_ortonormals, legend_note,
                     draw_normals=False, draw_speed_directions=False, subplot=subplot)
        if speed_subplot:
            plot_speed_results(control_points, control_points_speeds, legend_note, speed_subplot)

def run_multi_mds(extra_settings_set):
    settings = restore_settings()
    wing_fingure = plt.figure()
    subplot = wing_fingure.add_subplot(111)
    speed_pts = plt.figure()
    speed_subplot = speed_pts.add_subplot(111)
    for extra_settings in extra_settings_set:
        current_settings = settings.copy()
        current_settings.update(extra_settings)
        if settings['verbosity'] >= 1:
            print 'started calculation \033[4m%s\033[0m'%current_settings['discretisator_settings']['type']
        start_time = clock()
        run_mds(current_settings, subplot, speed_subplot)
        if settings['verbosity'] >= 1:
            print '\tcalculation took \033[31m%s\033[0m sec.'%(clock() - start_time)
    prop = fnt.FontProperties(size=10) 
    plt.legend(loc=2, ncol=1, borderaxespad=0., prop=prop)
    plt.show()
    
if __name__ == '__main__':
     if run_tests(ContourDiscretiseTestCase) and run_tests(DFMTestCase):
         extra_settings_set = [
             {
                'c' : 50,
                'discretisator_settings' : {
                    'type' : 'x_axis_uniform',
                    'coefficient' : 200,
                    }
             },
             # {

             #    'discretisator_settings' : {
             #        'type' : 'x_axis_nonuniform',
             #        'hmax' : 0.01,
             #        'hmin' : 0.001,
             #        'transition_length' : 0.6,
             #        }
             # },
             {
                 'c' : 50,
                 'discretisator_settings' : {
                    'type' : 'contour_nonuniform',
                    'hmax' : 0.1,
                    'hmin' : 0.01,
                    'transition_length' : 0.4,
                    }
             },
             ]
         run_multi_mds(extra_settings_set)
