#coding: utf-8
from math import sqrt

#from scipy.misc import derivative
from scipy.integrate import quad

def derivative(func, x, eps=1E-5):
    if x < 0.5:
        left_p = x
        right_p = x + eps
    else:
        right_p = x
        left_p = x - eps
    return (func(right_p) - func(left_p)) / eps

def curve_lenght(curve_equation, x_from, x_to):
    """Calculates 1D curve length from x_from to x_to"""
    func_to_integrate = lambda x: sqrt(1 + (derivative(curve_equation, x)) ** 2) 
    lenght = quad(func_to_integrate, x_from, x_to)
    return lenght[0]
