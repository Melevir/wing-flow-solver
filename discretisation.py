from math import e

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.lines as lines

from contour_functions import CONTOUR_FUNCTIONS
from utils import curve_lenght


def x_axis_uniform_discretisation(settings):
    #FIXME refactor
    contour_function, c, discretisation_coefficient = CONTOUR_FUNCTIONS[settings['contour_function']], settings['c'], settings['discretisator_settings']['coefficient']
    n = discretisation_coefficient
    x_vals = [1./n * i for i in xrange(n + 1)]
    contour_points = list()
    #all elements except last in reverse order
    for x in x_vals[::-1][1:]:
        contour_points.append([x, contour_function(x, c, '+')])
    for x in x_vals[1:]:
        contour_points.append([x, contour_function(x, c, '-')])
    return contour_points

def x_axis_nonuniform_discretisation(settings):
    #FIXME refactor
    contour_function, c = CONTOUR_FUNCTIONS[settings['contour_function']], settings['c']
    hmax, hmin = settings['discretisator_settings']['hmax'], settings['discretisator_settings']['hmin']
    nose_length, tail_length, transition_length = settings['nose_length'], settings['tail_length'], settings['discretisator_settings']['transition_length']
    x_vals = [0,]
    h_function = lambda x: x_axis_nonuniform_step_function(x, hmin, hmax, nose_length, tail_length, transition_length)
    while(x_vals[-1] < 1):
        h = h_function(x_vals[-1])
        if x_vals[-1] + h <= 1:
            x_vals.append(x_vals[-1] + h)
        else:
            break
    #FIXME won't work for any contours except default
    if x_vals[-1] < 1:
        x_vals.append(1)
    #FIXME copypaste
    steps = []
    for i, x in enumerate(x_vals):
        steps.append(x - x_vals[i - 1])
    if settings['verbosity'] >=1:
        print '\tmax x axis step = %s'%max(steps[1:])
        print '\tmin x axis step = %s'%min(steps[1:])
    contour_points = list()
    for x in x_vals[::-1][1:]:
        contour_points.append([x, contour_function(x, c, '+')])
    for x in x_vals[1:]:
        contour_points.append([x, contour_function(x, c, '-')])
    return contour_points

def contouor_nonuniform_discretisation(settings):
    #FIXME COPYPASTE!!
    full_contour_function, c = CONTOUR_FUNCTIONS[settings['contour_function']], settings['c']
    hmax, hmin = settings['discretisator_settings']['hmax'], settings['discretisator_settings']['hmin']
    nose_length, tail_length, transition_length = settings['nose_length'], settings['tail_length'], settings['discretisator_settings']['transition_length']
    contour_function = lambda x: full_contour_function(x, settings['c'], '+')
    x_vals = [0,]
    #TODO rename step function since its same for different methods
    h_function = lambda x: x_axis_nonuniform_step_function(x, hmin, hmax, nose_length, tail_length, transition_length)
    while(x_vals[-1] < 1):
        new_x = get_next_point_for_contour_nonuniform_discretisation(
            contour_function, x_vals[-1], h_function)
        #TODO update exit condition
        if new_x <= 1 and new_x + hmin <= 1:
            x_vals.append(new_x)
        else:
            break
    #FIXME won't work for any contours except default
    if x_vals[-1] < 1:
        x_vals.append(1)
    steps = []
    for i, x in enumerate(x_vals):
        steps.append(x - x_vals[i - 1])
    if settings['verbosity'] >=1:
        print '\tmax x axis step = %s'%max(steps[1:])
        print '\tmin x axis step = %s'%min(steps[1:])
    contour_points = list()
    for x in x_vals[::-1][1:]:
        contour_points.append([x, full_contour_function(x, c, '+')])
    for x in x_vals[1:]:
        contour_points.append([x, full_contour_function(x, c, '-')])
    return contour_points

def get_next_point_for_contour_nonuniform_discretisation(contour_function, current_x, h_function, eps=1E-10):
    perfect_dist = h_function(current_x)
    current_h = perfect_dist
    h_step = current_h / 10
    current_dist = curve_lenght(contour_function, current_x, current_x + current_h)
    prev_dist = current_dist
    while(abs(current_dist - perfect_dist) > eps):
        current_dist = curve_lenght(contour_function, current_x, current_x + current_h)
        if current_dist < perfect_dist:
            if prev_dist > perfect_dist:
                h_step /=2
            current_h += h_step
        else:
            if prev_dist < perfect_dist:
                h_step /=2
            current_h -= h_step
        prev_dist = current_dist
    return current_x + current_h

def x_axis_nonuniform_step_function(x, hmin, hmax, nose_length, tail_length, transition_length):
    half_trans = transition_length / 2.
    alpha  = hmax - hmin
    beta = 24. / transition_length
    sigmoid_fun = lambda x: sigmoid(x, alpha, beta)
    if x <= nose_length - half_trans:
        return hmin
    elif x <= nose_length + half_trans:
        return hmin + sigmoid_fun(x - nose_length)
    elif x <= tail_length - half_trans:
        return hmax
    elif x <= tail_length + half_trans:
        return hmax - sigmoid_fun(x - tail_length)
    else:
        return hmin
    
def sigmoid(x, alpha, beta):
    return alpha * 1. / (1. + e ** ( -1 * beta * x))
    
def discretise_contour(settings):
    discretisator =  DISCRETISATORS_TYPES[settings['discretisator_settings']['type']]
    return discretisator(settings)
        
#FIXME move to init.py
DISCRETISATORS_TYPES = {
    'x_axis_uniform' : x_axis_uniform_discretisation,
    'x_axis_nonuniform': x_axis_nonuniform_discretisation,
    'contour_nonuniform': contouor_nonuniform_discretisation,
    }
