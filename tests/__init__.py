import unittest


def run_tests(testcase):
    suite = unittest.TestLoader().loadTestsFromTestCase(testcase)
    return unittest.TextTestRunner(verbosity=0).run(suite).wasSuccessful()


