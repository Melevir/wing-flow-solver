import unittest
from math import sqrt

from dfm import *
from utils import curve_lenght
#TODO discretisation tests in separate file
from discretisation import *

EXTRA_TEST_SETTINGS = {
    'verbosity' : 0,
    'discretisator_settings' : {
        'type' : 'x_axis_uniform',
        'coefficient' : 10,
        }
    
    }

class SettingsUsingTestCase(unittest.TestCase):
    def setUp(self):
        self.settings = restore_settings()
        self.settings.update(EXTRA_TEST_SETTINGS)

class DFMTestCase(SettingsUsingTestCase):
    def setUp(self):
        self.settings = restore_settings()
        self.wirldwind_points = discretise_contour(self.settings)
        self.control_points, self.control_points_ortonormals = calculate_control_points(self.wirldwind_points)
        
    def test_contron_points_amount_is_correct(self):
        self.assertEqual(len(self.wirldwind_points), len(self.control_points))

    def test_normals_are_units(self):
        for n in self.control_points_ortonormals:
            n_len = sqrt(n[0] ** 2 + n[1] ** 2)
            self.assertAlmostEqual(n_len, 1)

    def test_norlams_looks_outside_body(self):
        for i, n in enumerate(self.control_points_ortonormals):
            self.assertTrue(n[1] * self.control_points[i][1] >= 0)

    def test_normals_are_normal_to_control_points(self):
        for i, n in enumerate(self.control_points_ortonormals):
            cp = self.control_points[i]
            nwp = self.wirldwind_points[i + 1] if i != len(self.wirldwind_points) - 1 else self.wirldwind_points[0]
            vect = nwp[0] - cp[0], nwp[1] - cp[1]
            scalar = np.dot(n, vect)
            self.assertAlmostEqual(scalar, 0)

    def test_control_points_are_in_the_middle_of_wirldwind_points(self):
        for i, cp in enumerate(self.control_points):
            prev_wp = self.wirldwind_points[i]
            next_wp = self.wirldwind_points[i + 1] if i != len(self.wirldwind_points) - 1 else self.wirldwind_points[0]
            self.assertAlmostEqual((prev_wp[0] + next_wp[0]) / 2, cp[0])
            self.assertAlmostEqual((prev_wp[1] + next_wp[1]) / 2, cp[1])
            
    def test_laes_sizes_are_correct(self):
        laes_size = len(self.control_points)
        A, b = fill_laes(self.settings['Vinf'], self.wirldwind_points, self.control_points, self.control_points_ortonormals)
        self.assertEqual(len(A), laes_size)
        self.assertEqual(len(b), laes_size)
        for row in A:
            self.assertEqual(len(row), laes_size)            

    def test_laes_last_row_consists_of_ones(self):
        if self.settings['make_regularisation']:
            laes_size = len(self.control_points)
            A, b = fill_laes(self.settings['Vinf'], self.wirldwind_points, self.control_points, self.control_points_ortonormals)
            for row in A:
                self.assertEqual(row[-1], 1)

    def test_W_calcutales_correct_values(self):
        test_vals = (
            ((0, 1),(0, 0)),
            ((1, 0),(0, 1)),
            ((pi, 0),(0, 2)),
            )
        expected_vals = (
            (-0.159, 0.0),
            (0.079, 0.079),
            (0.0229, 0.036),
            )
        for i, (m, x) in enumerate(test_vals):
            res = W(m, x)
            self.assertEqual(len(res), 2)
            for j, val in enumerate(res):
                self.assertAlmostEqual(val, expected_vals[i][j], 2)
            
        
            
class ContourDiscretiseTestCase(SettingsUsingTestCase):
   def setUp(self):
       self.settings = restore_settings()

   def test_contour_nonuniform_function_works_fine(self):
       current_x = 4
       h_function = lambda x : 0.5
       contour_function = lambda x : sqrt(x)
       next_x = get_next_point_for_contour_nonuniform_discretisation(
           contour_function, current_x, h_function)
       curve_dist = curve_lenght(contour_function, current_x, next_x)
       self.assertAlmostEqual(curve_dist, h_function(current_x))
