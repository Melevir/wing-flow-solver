#TODO name of contour?
def default_contour_function(x, c=30, sign='+'):
    part1 = 0.25 * (0 - x + 7 * x ** 2 - 6 * x ** 3 )
    part2 = x ** 0.87 * (1 - x ** 2) ** 0.56
    if sign == '+':
        summ = part1 + part2
    else:
        summ = part1 - part2
    return c * 1. / 100 * summ

CONTOUR_FUNCTIONS = {
    'default': default_contour_function
    }

